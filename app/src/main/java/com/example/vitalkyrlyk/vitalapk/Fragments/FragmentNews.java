package com.example.vitalkyrlyk.vitalapk.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.vitalkyrlyk.vitalapk.Activities.AddNews;
import com.example.vitalkyrlyk.vitalapk.Activities.DrawerActivity;
import com.example.vitalkyrlyk.vitalapk.R;
import com.example.vitalkyrlyk.vitalapk.RVAdapter.RVRealmAdapter;
import com.example.vitalkyrlyk.vitalapk.RealmBD.NewsObject;

import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class FragmentNews extends Fragment {

    View view;
    DrawerActivity drawerActivity;
    Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Realm.init(getContext());

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        drawerActivity = (DrawerActivity) activity;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, null);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycleView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        realm = Realm.getDefaultInstance();
        RealmResults<NewsObject> newsObjectRealmResults = realm.where(NewsObject.class).findAll();
        RealmRecyclerViewAdapter adapter = new RVRealmAdapter(newsObjectRealmResults, true);
        recyclerView.setAdapter(adapter);

//        RealmResults<NewsObject> newsObjectRealmResults = realm.where(PersonObject.class).findAll();


//        RealmResults<PersonObject> resultLog = realm.where(PersonObject.class).findAll();
//        for (PersonObject personObject : resultLog) {
//            if (drawerActivity.login.equals(personObject.getLogin()) &&
//                    drawerActivity.password.equals(personObject.getPass())) {
//                RealmResults<NewsObject> newsObjects = personObject.getNewsObjectsList().where().findAll();
//                RealmRecyclerViewAdapter adapter = new RVRealmAdapter(newsObjects, true);
//                recyclerView.setAdapter(adapter);
//
//            }
//        }


//                RealmRecyclerViewAdapter adapter = new RvAdapter(newsObjectRealmResults, true);
//        recyclerView.setAdapter(adapter);

        Button btnAddNews = (Button) view.findViewById(R.id.btnAddNews);
        btnAddNews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btnAddNews:
                        Intent intent = new Intent(getActivity(), AddNews.class);
                        startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
