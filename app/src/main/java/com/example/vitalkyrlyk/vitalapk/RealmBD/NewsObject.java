package com.example.vitalkyrlyk.vitalapk.RealmBD;


import io.realm.RealmObject;

public class NewsObject extends RealmObject {

    private String News;
    private String Image;

    public String getNews() {
        return News;
    }

    public void setNews(String News) {
        this.News = News;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }
}
