package com.example.vitalkyrlyk.vitalapk.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vitalkyrlyk.vitalapk.Fragments.FragmentInfo;
import com.example.vitalkyrlyk.vitalapk.Fragments.FragmentNews;
import com.example.vitalkyrlyk.vitalapk.R;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final String TAG = "myLogs";
    public String login, password, name, email, secName, date, image;
    SharedPreferences sPref;
    int them;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(them);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        Log.d(TAG, "Theme= " + them);

        Toolbar toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

//        Realm.init(this);
//        realm = Realm.getDefaultInstance();
//        realm.beginTransaction();
//        RealmResults<NewsObject> realmResults = realm.where(NewsObject.class).findAll();
//        Log.d("myLogs1", " lolo" + realmResults);
//        realm.commitTransaction();

        sPref = getSharedPreferences("Users", MODE_PRIVATE);
        login = sPref.getString("Login", "");
        password = sPref.getString("Pass", "");
        name = sPref.getString("Name", "");
        email = sPref.getString("Email", "");
        secName = sPref.getString("SecName", "");
        date = sPref.getString("Date", "");
        image = sPref.getString("Image", "");
        Uri uri = Uri.parse(sPref.getString("Image", ""));

        Log.d(TAG, "getString login: " + login + "; Pass: " + password);

        getSupportActionBar().setTitle(name);
        if ((login.length() < 4) && (password.length() < 8)) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, login, Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.profile_image);
//        circleImageView.setImageBitmap(BitmapFactory.decodeFile(sPref.getString("Image","")));
        circleImageView.setImageURI(uri);

        Log.d(TAG, "setImage Uri: " + uri);

        View view = navigationView.getHeaderView(0);
        TextView drawerLogin = (TextView) view.findViewById(R.id.drawerLogin);
        TextView drawerEmail = (TextView) view.findViewById(R.id.drawerEmail);
        ImageView drawerImage = (ImageView) view.findViewById(R.id.drawerImage);
        drawerImage.setImageURI(uri);
        drawerLogin.setText(login);
        drawerEmail.setText(email);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.titleA, FragmentNews.class)
                .add(R.string.titleB, FragmentInfo.class)
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {

            startActivity(new Intent(this, DrawerActivity.class));

        } else if (id == R.id.nav_screen) {

            if (them == R.style.AppTheme_Base) {
                them = R.style.AppThemeBase1;
            } else if (them == R.style.AppThemeBase1) {
                them = R.style.AppTheme_Base;
            }
            Log.d(TAG, "Theme= " + them);

            Intent intent = new Intent(this, DrawerActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            SharedPreferences.Editor ed = sPref.edit();
            ed.clear();
            ed.apply();
            login = sPref.getString("Login", "");
            password = sPref.getString("Pass", "");
            Log.d(TAG, "Navigation logout clear user; login is: " + login + "; pass is: " + password);
            startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "DrawerLayout On start");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "DrawerLayout On Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "DrawerLayout On Destroy");
    }
}
