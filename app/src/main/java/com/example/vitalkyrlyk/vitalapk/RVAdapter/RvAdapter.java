//package com.example.vitalkyrlyk.vitalapk.RVAdapter;
//
//import android.annotation.SuppressLint;
//import android.net.Uri;
//
//import android.support.annotation.Nullable;
//import android.support.v7.widget.CardView;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.vitalkyrlyk.vitalapk.R;
//import com.example.vitalkyrlyk.vitalapk.RealmBD.NewsObject;
//import com.example.vitalkyrlyk.vitalapk.RealmBD.PersonObject;
//
//import io.realm.OrderedRealmCollection;
//import io.realm.Realm;
//import io.realm.RealmRecyclerViewAdapter;
//
//
//public class RvAdapter extends RealmRecyclerViewAdapter<PersonObject, RvAdapter.NewsViewHolder>{
//
//    private Realm realm;
//    private OrderedRealmCollection<PersonObject> newsObjectRealmResults;
//
//    public RvAdapter(@Nullable OrderedRealmCollection<PersonObject> data, boolean autoUpdate) {
//        super(data, autoUpdate);
//        this.newsObjectRealmResults = data;
//    }
//
//
//    @Override
//    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        @SuppressLint("InflateParams") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, null);
//        NewsViewHolder newsViewHolder = new NewsViewHolder(view);
//        realm = Realm.getDefaultInstance();
//        return newsViewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(NewsViewHolder holder, int position) {
//
//        newsObjectRealmResults = realm.where(PersonObject.class).findAll();
//        PersonObject personObject = newsObjectRealmResults.get(position);
//        String news = personObject.getNewsObjectsList().get(position).getNews();
//        if (news!= null) {
//            holder.textNews.setText(personObject.getNewsObjectsList().get(position).getNews());
//        }
////        NewsObject newsObject = newsObjectRealmResults.get(position);
////        holder.textNews.setText(newsObject.getNews());
////        String image = newsObject.getImage();
//        String image = personObject.getNewsObjectsList().get(position).getImage();
//        if(image != null){
//            Uri imageUri = Uri.parse(image);
//            holder.photoNews.setImageURI(imageUri);
//        }
//    }
//
//    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
//        super.onAttachedToRecyclerView(recyclerView);
//    }
//
//    @Override
//    public int getItemCount() {
//        return isDataValid() ? newsObjectRealmResults.size() : 0;
//    }
//
//    static class NewsViewHolder extends RecyclerView.ViewHolder{
//        CardView cv;
//        TextView textNews;
//        ImageView photoNews;
//        NewsViewHolder(View itemView) {
//            super(itemView);
//            cv = (CardView)itemView.findViewById(R.id.cv);
//            textNews = (TextView)itemView.findViewById(R.id.textNews);
//            photoNews = (ImageView)itemView.findViewById(R.id.photoNews);
//        }
//    }
//
//    private boolean isDataValid() {
//        return newsObjectRealmResults != null && newsObjectRealmResults.isValid();
//    }
//}