package com.example.vitalkyrlyk.vitalapk.RealmBD;


import io.realm.RealmList;
import io.realm.RealmObject;

public class PersonObject extends RealmObject {

    private String name;
    private String SecName;
    private String Login;
    private String Email;
    private String Pass;
    private String Date;
    private String PassConfirm;
    private String Image;

    private RealmList<NewsObject> newsObjectsList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecName() {
        return SecName;
    }

    public void setSecName(String SecName) {
        this.SecName = SecName;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getPassConfirm() {
        return PassConfirm;
    }

    public void setPassConfirm(String PassConfirm) {
        this.PassConfirm = PassConfirm;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public RealmList<NewsObject> getNewsObjectsList() {
        return newsObjectsList;
    }

    public void setNewsObjectsList(RealmList<NewsObject> newsObjectsList) {
        this.newsObjectsList = newsObjectsList;
    }
}

