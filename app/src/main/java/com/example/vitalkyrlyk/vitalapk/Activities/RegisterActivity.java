package com.example.vitalkyrlyk.vitalapk.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vitalkyrlyk.vitalapk.R;
import com.example.vitalkyrlyk.vitalapk.RealmBD.PersonObject;

import io.realm.Realm;
import io.realm.RealmResults;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_PICTURE = 1;
    final String TAG = "myLogs";
    EditText etName, etSecName, etDate, etLogin, etEmail, etPass, etPassConfirm;
    Button btnRegisterRegistAc, setPng;
    Realm realm;
    PersonObject person = new PersonObject();
    private String selectedImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        etName = (EditText) findViewById(R.id.etName);
        etSecName = (EditText) findViewById(R.id.etSecName);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPass = (EditText) findViewById(R.id.etPass);
        etPassConfirm = (EditText) findViewById(R.id.etPassConfirm);
        etDate = (EditText) findViewById(R.id.etDate);

        btnRegisterRegistAc = (Button) findViewById(R.id.btnRegisterRegistAc);
        btnRegisterRegistAc.setOnClickListener(this);
        setPng = (Button) findViewById(R.id.setPng);
        setPng.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int error = 1;

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        switch (v.getId()) {
            case R.id.btnRegisterRegistAc:

                if (etName.getText().length() == 0) {
                    etName.setError("This field must not be empty");
                    error = 0;
                } else {
                    person.setName(etName.getText().toString());
                }

                if (etSecName.getText().length() == 0) {
                    etSecName.setError("This field must not be empty");
                    error = 0;
                } else {
                    person.setSecName(etSecName.getText().toString());
                }

                if ((etLogin.getText().length() == 0) || (etLogin.getText().length() < 4) ||
                        (etLogin.getText().length() > 12)) {
                    etLogin.setError("This field must be 4-12 symbol");
                    error = 0;
                } else {
                    person.setLogin(etLogin.getText().toString());
                }

                RealmResults<PersonObject> resultLog = realm.where(PersonObject.class).findAll();
                Log.d(TAG, "Result login: " + resultLog);
                for (PersonObject personObject : resultLog) {
                    if (etLogin.getText().toString().equals(personObject.getLogin())) {
                        error = 0;
                        etLogin.setError("This login already used");
                    }
                }


                if (etEmail.getText().length() == 0) {
                    etEmail.setError("This field must not be empty");
                    error = 0;
                } else {
                    person.setEmail(etEmail.getText().toString());
                }

                if ((etPass.getText().length() == 0) || (etPass.getText().length() < 8) ||
                        (etPass.getText().length() > 16)) {
                    etPass.setError("This field must be 8-16 symbol");
                    error = 0;
                } else {
                    person.setPass(etPass.getText().toString());
                }

                if (etPassConfirm.getText().toString().equals(etPass.getText().toString())) {
                    person.setPassConfirm(etPassConfirm.getText().toString());
                } else {
                    etPassConfirm.setError("Passwords do not match");
                    error = 0;
                }

                if (etDate.getText().length() == 0) {
                    etDate.setError("This field must not be empty");
                    error = 0;
                } else {
                    person.setDate(etDate.getText().toString());
                }

                if (selectedImagePath == null) {
//                    error = 0;
                    Toast.makeText(this, "Not image", Toast.LENGTH_SHORT).show();
                } else {
                    person.setImage(selectedImagePath);
                    Log.d(TAG, "setImage: " + selectedImagePath);
                }
                if (error == 1) {

                    realm.beginTransaction();
                    realm.insert(person);
                    realm.commitTransaction();

                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                    Log.d(TAG, "person add. Name = " + person.getName() + "; second name = " + person.getSecName() +
                            "; Login = " + person.getLogin() + "; Email = " + person.getEmail() + "; Password = " + person.getPass() +
                            "; Date of Birth = " + person.getDate() + "; Image: " + person.getImage());
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.setPng:
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);

                Log.d(TAG, "Image set png:" + selectedImageUri.getPath());
                Log.d(TAG, "Image set Path:" + selectedImagePath);
            } else {
                Toast.makeText(this, "Error image", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error image");
            }
        }
    }


    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {

            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        return uri.getPath();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
