package com.example.vitalkyrlyk.vitalapk.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vitalkyrlyk.vitalapk.R;
import com.example.vitalkyrlyk.vitalapk.RealmBD.PersonObject;

import io.realm.Realm;
import io.realm.RealmResults;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    final String TAG = "myLogs";
    final String SAVED_Login = "Login";
    final String SAVED_Name = "Name";
    final String SAVED_SecName = "SecName";
    final String SAVED_Email = "Email";
    final String SAVED_Pass = "Pass";
    final String SAVED_Date = "Date";
    final String SAVED_Image = "Image";
    EditText etLoginLogAct, etPassLogAct;
    Button btnLogInLogAct, btnRegisterLogAct;
    Realm realm;
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etLoginLogAct = (EditText) findViewById(R.id.etLoginLogAct);
        etPassLogAct = (EditText) findViewById(R.id.etPassLogAct);

        btnLogInLogAct = (Button) findViewById(R.id.btnLogInLogAct);
        btnRegisterLogAct = (Button) findViewById(R.id.btnRegisterLogAct);

        btnLogInLogAct.setOnClickListener(this);
        btnRegisterLogAct.setOnClickListener(this);

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        sPref = getSharedPreferences("Users", MODE_PRIVATE);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnLogInLogAct:

                int error = 1;

                RealmResults<PersonObject> resultLog = realm.where(PersonObject.class).findAll();
                for (PersonObject personObject : resultLog) {
                    if (etLoginLogAct.getText().toString().equals(personObject.getLogin()) &&
                            etPassLogAct.getText().toString().equals(personObject.getPass())) {
                        error = 0;

                        saveUser();
                    }
                }
                if (error == 1) {
                    Toast.makeText(this, "Not found login or password not correctly", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(this, DrawerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;


            case R.id.btnRegisterLogAct:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        Log.d(TAG, "On destroy login activity");
    }

    private void saveUser() {

        RealmResults<PersonObject> resultLog = realm.where(PersonObject.class).findAll();
        Log.d(TAG, "Result log: " + resultLog);
        for (PersonObject personObject : resultLog) {
            if (etLoginLogAct.getText().toString().equals(personObject.getLogin()) &&
                    etPassLogAct.getText().toString().equals(personObject.getPass())) {


                SharedPreferences.Editor ed = sPref.edit();

                ed.putString(SAVED_Login, personObject.getLogin());
                ed.putString(SAVED_Pass, personObject.getPass());
                ed.putString(SAVED_Email, personObject.getEmail());
                ed.putString(SAVED_Name, personObject.getName());
                ed.putString(SAVED_SecName, personObject.getSecName());
                ed.putString(SAVED_Date, personObject.getDate());
                ed.putString(SAVED_Image, personObject.getImage());
                ed.apply();

                Log.d(TAG, "shared save: " + sPref.getString(SAVED_Login, "") + " ; " +
                        sPref.getString(SAVED_Pass, "") + " ; " + sPref.getString(SAVED_Email, "") + " ; " +
                        sPref.getString(SAVED_Name, "") + " ; " + sPref.getString(SAVED_SecName, "")
                        + " ; " + sPref.getString(SAVED_Date, "") + " ; " + sPref.getString(SAVED_Image, ""));
            }
        }
    }

}