package com.example.vitalkyrlyk.vitalapk.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vitalkyrlyk.vitalapk.Activities.DrawerActivity;
import com.example.vitalkyrlyk.vitalapk.R;

public class FragmentInfo extends Fragment {
    final String TAG = "myLogs";
    DrawerActivity drawerActivity;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        drawerActivity = (DrawerActivity) activity;
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_info, null);

        TextView frameLoginInfo = (TextView) view.findViewById(R.id.frameLoginInfo);
        TextView frameNameInfo = (TextView) view.findViewById(R.id.frameNameInfo);
        TextView frameSecNameInfo = (TextView) view.findViewById(R.id.frameSecNameInfo);
        TextView frameEmailInfo = (TextView) view.findViewById(R.id.frameEmailInfo);
        TextView frameDataInfo = (TextView) view.findViewById(R.id.frameDataInfo);
        frameLoginInfo.setText("Login: " + drawerActivity.login);
        frameNameInfo.setText("Name: " + drawerActivity.name);
        frameSecNameInfo.setText("Second Name: " + drawerActivity.secName);
        frameEmailInfo.setText("Email: " + drawerActivity.email);
        frameDataInfo.setText("Date: " + drawerActivity.date);
        Log.d(TAG, "fragment info on onCreate");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "fragment info onStart");
    }
}
