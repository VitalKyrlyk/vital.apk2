package com.example.vitalkyrlyk.vitalapk.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vitalkyrlyk.vitalapk.R;
import com.example.vitalkyrlyk.vitalapk.RealmBD.NewsObject;

import io.realm.Realm;
import io.realm.RealmResults;


public class AddNews extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_PICTURE = 1;
    final String SAVED_News = "News";
    final String SAVED_NewsImage = "NewsImage";
    final String TAG = "myLogs";
    SharedPreferences sharedPreferences;
    EditText etNews;
    Button btnAddImageNews, btnCreateNews;
    Realm realm;
    private String selectedImagePath;
//    NewsObject newsObject = new NewsObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_news);

        etNews = (EditText) findViewById(R.id.etNews);
        btnAddImageNews = (Button) findViewById(R.id.btnAddImageNews);
        btnCreateNews = (Button) findViewById(R.id.btnCreateNews);
        btnAddImageNews.setOnClickListener(this);
        btnCreateNews.setOnClickListener(this);

        sharedPreferences = getSharedPreferences("addNews", MODE_PRIVATE);
        realm = Realm.getDefaultInstance();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnAddImageNews:

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);

                break;

            case R.id.btnCreateNews:

                SharedPreferences.Editor ed = sharedPreferences.edit();
                ed.putString(SAVED_News, etNews.getText().toString());
                ed.putString(SAVED_NewsImage, selectedImagePath);
                ed.apply();
                Log.d("myLogs", sharedPreferences.getString("SAVED_News", "") + "NEWS 1");

                NewsObject newsObject = new NewsObject();
//                RealmList<NewsObject> newsObject = new RealmList<>();
                newsObject.setNews(etNews.getText().toString());
                newsObject.setImage(selectedImagePath);

                realm.beginTransaction();
                realm.insert(newsObject);
                realm.commitTransaction();

                RealmResults<NewsObject> newsObjectRealmResults = realm.where(NewsObject.class).findAll();
                Log.d(TAG, "Result news: " + newsObjectRealmResults);

                Intent intent1 = new Intent(this, DrawerActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                Log.d(TAG, "btnCreate Image set Path:" + selectedImagePath);
                Log.d(TAG, "btnCreate Image set Path: 2" + sharedPreferences.getString("SAVED_NewsImage", ""));

                break;

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                assert selectedImageUri != null;
                selectedImagePath = selectedImageUri.getPath();

                Log.d(TAG, "Image set png:" + selectedImageUri.getPath());
                Log.d(TAG, "Image set Path:" + selectedImagePath);
            } else {
                Toast.makeText(this, "Error image", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error image");
            }
        }
    }


    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        return uri.getPath();
    }
}