package com.example.vitalkyrlyk.vitalapk.Splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.vitalkyrlyk.vitalapk.Activities.DrawerActivity;
import com.example.vitalkyrlyk.vitalapk.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, DrawerActivity.class);
                startActivity(i);
                finish();
            }
        }, 1500);
    }

    @Override
    public void onClick(View v) {


    }


}
